boxworld = {}

function boxworld.show_nextlevel_form(name, lvl)
	local gui = "size[3,1.1]" ..
	"label[0.0,-0.2;You finished level " .. lvl .." !]" ..
	"label[0.0,0.1;Will you finish level " .. (lvl+1) .." ?]" ..
	"button[1.0,0.6;1.0,1.0;ok;GO!]"
	minetest.show_formspec(name, "boxworld.popup", gui)
end

function boxworld.show_finallevel_form(name, lvl)
	local gui = "size[3,1.1]" ..
	"label[0.0,-0.2;You finished all " .. lvl .." levels!]" ..
	"label[0.0,0.1;Congratulations !!!]" ..
	"button[1.0,0.6;1.0,1.0;ok;OK]"
	minetest.show_formspec(name, "boxworld.popup", gui)
end

function boxworld.get_inventory_formspec()
	local levels = ""
	for i = 1,level.max_level-1 do
		levels = levels .. i .. ","
	end
	levels = levels .. level.max_level
	return "size[4,1.6]" ..
	"label[0.0,-0.2;Level " .. level.number .. "]" ..
	"button[0.0,0.2;3.0,1.0;restart;Restart Level]" ..
	"button[0.0,1.0;3.0,1.0;jump;Jump to Level:]" ..
	"dropdown[3.0,1.0;1.0;lvl;"..levels..";"..level.number.."]"
end

function boxworld.finish_level(pn)
	local next_level = level.number + 1
	local old_level = level.number
	local is_next_level = false
	if level.level_exists("level" .. next_level) then
		level.number = next_level
		if level.number > level.max_level then
			level.max_level = level.number
		end
		is_next_level = true
	end
	level.load_level("level" .. level.number)
	
	if is_next_level then
		boxworld.show_nextlevel_form(pn, old_level)
	else
		boxworld.show_finallevel_form(pn, old_level)
	end
end

minetest.register_on_player_receive_fields(function(player, form, pressed)
	if form == "" then
		if pressed.restart then
			level.restart_level()
			minetest.close_formspec(player:get_player_name(), form)
		elseif pressed.jump then
			level.number = tonumber(pressed.lvl)
			level.load_level("level" .. level.number)
			minetest.close_formspec(player:get_player_name(), form)
		end
	end
	if form == "boxworld.popup" then
		minetest.close_formspec(player:get_player_name(), form)
		return
	end
end)

local moving_box = {
	initial_properties = {
		physical = false,
		collisionbox = {-0.5, -0.5, -0.5, 0.5, 0.5, 0.5},
		visual_size = {x=0.6667, y=0.6667, z=0.6667},
		visual = "item",
		wield_item = "boxworld:box",
	},
}

minetest.register_entity("boxworld:moving_box", moving_box)

discrete_player.make_player({
	-- Properties of the entity
	initial_properties = {
		physical = false,
		collisionbox = {-0.5, -0.5, -0.5, 0.5, 0.5, 0.5},
		visual = "mesh",
		mesh = "boxworld_empty_model.obj",
	},
	
	my_box = nil,
	walk_speed = 2,
	
	on_walk_to = function(self, new_pos, direction)
		local node = minetest.get_node(new_pos).name
		if not minetest.registered_nodes[node].walkable then
			return true
		end
		if node == "boxworld:box" or node == "boxworld:boxr" then
			self.box_target = vector.add(new_pos, self.directions[direction])
			local node2 = minetest.get_node(self.box_target).name
			if minetest.registered_nodes[node2].walkable then
				return false
			else
				--move the box
				if node == "boxworld:boxr" then
					minetest.set_node(new_pos, {name="boxworld:marble"})
					level.boxes = level.boxes + 1
				else
					minetest.set_node(new_pos, {name="air"})
				end

				self.my_box = minetest.add_entity(new_pos, "boxworld:moving_box")
				self.red_box = node2 == "boxworld:marble"
				if self.red_box then
					self.my_box:set_properties({wield_item = "boxworld:boxr"})
				end
				self.my_box:set_velocity(vector.multiply(self.directions[direction], self.walk_speed))
				return true
			end
		end
	end,
	
	on_walk_finish = function(self)
		--place the box if pushing any
		if self.my_box then
			local old_box = self.my_box
			self.my_box = nil
			if self.red_box then
				minetest.set_node(self.box_target, {name="boxworld:boxr"})
				level.boxes = level.boxes - 1
				if level.boxes == 0 then
					self.active = false
					minetest.after(0.1, boxworld.finish_level, self.player)
				end
			else
				minetest.set_node(self.box_target, {name="boxworld:box"})
			end
			minetest.after(0.1, function() old_box:remove() end)
		end
	end,
	
	after_attach = function(self)
		local player = minetest.get_player_by_name(self.player)
		player:set_inventory_formspec(boxworld.get_inventory_formspec())
		player:hud_set_flags({
			hotbar = false,
			healthbar = false,
			crosshair = false,
			wielditem = false,
			minimap = false,
			minimap_radar = false,
		})
		local inv = player:get_inventory()
		for i = 1,inv:get_size("main") do
			inv:set_stack("main", i, ItemStack(nil))
		end
	end,
})

level.level_path = minetest.get_modpath("boxworld") .. "/levels/"

function level.before_load(name)
	if discrete_player.current then
		discrete_player.current.active = false
	end
end

function level.after_load(name)
	if discrete_player.current then
		discrete_player.current.object:set_pos(level.start_pos)
		discrete_player.current.active = true
		local player = minetest.get_player_by_name(discrete_player.current.player)
		player:set_inventory_formspec(boxworld.get_inventory_formspec())
	else
		local player = minetest.get_player_by_name("singleplayer")
		player:set_pos(level.start_pos)
	end
end

function level.before_export(pos1, pos2, name)
	local boxes = 0
	level.start_pos = {x=1, y=1, z=1}
	level.pos1 = pos1
	for x = pos1.x, pos2.x do
		for y = pos1.y, pos2.y do
			for z = pos1.z, pos2.z do
				node = minetest.get_node_or_nil({x=x, y=y, z=z})
				if node.name == "boxworld:box" then
					boxes = boxes + 1
				elseif node.name == "boxworld:start" then
					level.start_pos = vector.subtract(vector.new(x, y, z), pos1)
					minetest.set_node(vector.new(x, y, z), {name="air"})
				end
			end
		end
	end
	return string.format("level.start_pos = {x = %d, y = %d, z = %d}\nlevel.boxes = %d",
	                      level.start_pos.x, level.start_pos.y, level.start_pos.z, boxes)
end

function level.after_export(pos1, pos2, name)
	minetest.set_node(vector.add(level.start_pos, level.pos1), {name="boxworld:start"})
end

function level.before_import(name)

end

function level.after_import(name)
	minetest.set_node(level.start_pos, {name="boxworld:start"})
end

function level.save_game(file)
	if level.number == nil then
		level.number = 1
	end
	if level.max_level == nil then
		level.max_level = 1
	end

	file:write("level.number = "..level.number.."\n")
	file:write("level.max_level = "..level.max_level.."\n")
	file:flush()
end

minetest.register_on_joinplayer(function(player)
	player:override_day_night_ratio(1)
	player:set_properties({visual_size = {x=0.5, y=0.5, z=0.5}, collisionbox = {-0.2, 0.0, -0.2, 0.2, 0.7, 0.2}})
	player:set_eye_offset({x=0,y=-12,z=0}, {x=0,y=-12,z=0})
	local name = player:get_player_name()
	local privs = minetest.get_player_privs(name)
	privs.fly = true
	privs.fast = true
	privs.noclip = true
	minetest.set_player_privs(name, privs)
end)

minetest.register_node("boxworld:marble", {
	description = "Marble",
	tiles = {"boxworld_marble.png"},
	drawtype = "mesh",
	mesh = "boxworld_marble.obj",
	paramtype = "light",
	sunlight_propagates = true,
	walkable = false,
	groups = {cracky = 3},
})

minetest.register_node("boxworld:bricks", {
	description = "Bricks",
	tiles = {"boxworld_bricks.png"},
	paramtype = "light",
	sunlight_propagates = true,
	groups = {cracky = 3},
})

minetest.register_node("boxworld:box", {
	description = "Box",
	tiles = {"boxworld_box.png"},
	paramtype = "light",
	sunlight_propagates = true,
	groups = {cracky = 3},
})

minetest.register_node("boxworld:boxr", {
	description = "Red Box",
	tiles = {"boxworld_boxr.png"},
	paramtype = "light",
	sunlight_propagates = true,
	groups = {cracky = 3},
})

minetest.register_node("boxworld:start", {
	description = "Start",
	tiles = {"boxworld_start.png"},
	paramtype = "light",
	drawtype = "glasslike",
	sunlight_propagates = true,
	walkable = false,
	groups = {cracky = 3},
})

